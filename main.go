// main.go

package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/pelasonny1/datos-abiertos-clima/weather-services/temperature/handler"
	"gitlab.com/pelasonny1/datos-abiertos-clima/weather-services/temperature/middleware"
)

func main() {
	http.HandleFunc("/temperatura", middleware.EnableCORS(handler.TemperaturaHandler))

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080" // Puerto por defecto
	}
	log.Printf("Servidor corriendo en el puerto %s", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
