package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestTemperaturaHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/temperatura", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(TemperaturaHandler)

	handler.ServeHTTP(rr, req)

	// Verificar el código de estado
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler devolvió un código incorrecto: obtuvo %v esperaba %v",
			status, http.StatusOK)
	}

	// Verificar la respuesta
	expected := `{"temperatura":20}`
	if rr.Body.String() != expected {
		t.Errorf("handler devolvió una respuesta inesperada: obtuvo %v esperaba %v",
			rr.Body.String(), expected)
	}
}
