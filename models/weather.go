package models

// WeatherResponse estructura para mapear solo los datos que necesitamos de la respuesta
type WeatherResponse struct {
	Location struct {
		Name string `json:"name"`
	} `json:"location"`
	Current struct {
		TempC float64 `json:"temp_c"`
	} `json:"current"`
}
