// services/weatherService_test.go

package services

import (
	"encoding/json"
	"net/http"
)

func TemperaturaHandler(w http.ResponseWriter, r *http.Request) {
	apiKey := "5568f7e75ca94554a5f200136240904"
	ciudad := "Buenos%20Aires"

	// Pasa http.DefaultClient como el primer argumento
	weatherData, err := ObtenerTemperatura(http.DefaultClient, apiKey, ciudad)
	if err != nil {
		http.Error(w, "Error al obtener los datos del clima", http.StatusInternalServerError)
		return
	}

	respuesta := map[string]float64{"temperatura": weatherData.Current.TempC}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(respuesta)
}
