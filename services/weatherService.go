// services/weatherService.go

package services

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/pelasonny1/datos-abiertos-clima/weather-services/temperature/models"
)

func ObtenerTemperatura(client *http.Client, apiKey, ciudad string) (*models.WeatherResponse, error) {
	url := "http://api.weatherapi.com/v1/current.json?key=" + apiKey + "&q=" + ciudad + "&aqi=no"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("Error creando la solicitud HTTP: %v", err)
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Error al hacer la solicitud a Weather API: %v", err)
		return nil, err
	}
	defer resp.Body.Close()

	// Leer el cuerpo para el diagnóstico de errores
	body, _ := ioutil.ReadAll(resp.Body)

	var weatherResp models.WeatherResponse
	if err := json.Unmarshal(body, &weatherResp); err != nil {
		log.Printf("Error al decodificar la respuesta: %v", err)
		log.Printf("Cuerpo de la respuesta: %s", string(url))
		return nil, err
	}

	return &weatherResp, nil
}
